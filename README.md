# Memory Game #


### Technologies Used: ###
* HTML
* CSS
* JavaScript
  * jQuery


[User stories/ wireframes](https://trello.com/b/4v85pa1y/kate-project-1-memory-browser-game)

### Additional Planned Optional Features: ###
* Timer
* Two player game
* Progress bar
* Track correct matches
* Replace characters with images.
* Increase difficulty by adding more tiles or by making tiles more similar as the game progresses.


### How to Contribute: ###
Fork project and make a pull request with contributions.